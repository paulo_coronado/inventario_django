from attr import fields
from django.forms import CharField, Form, Textarea, ModelForm

from gestor_elementos.models import Categoria

class CategoriaForm(ModelForm):

    class Meta:
        model=Categoria
        fields=["nombre", "descripcion"]
    
