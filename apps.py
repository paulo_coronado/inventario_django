from django.apps import AppConfig


class GestorElementosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gestor_elementos'
