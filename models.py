from django.db.models import *

# Create your models here.
class Categoria(Model):
    nombre=CharField(max_length=50)
    descripcion=TextField()

class Producto(Model):

    #Listado de atributos
    #De manera predeterminada un modelo define un campo único llamado ID
    id_categoria=ForeignKey(Categoria, on_delete=CASCADE)
    GTIN=CharField(max_length=100)
    nombre=CharField(max_length=100)
    descripcion=TextField()
    marca=CharField(max_length=100)
    peso=FloatField()
    dimensiones=CharField(max_length=50)

class Bodega(Model):
    nombre=CharField(max_length=50)
    descripcion=TextField()
    direccion=CharField(max_length=150)
    pais=CharField(max_length=50)
    departamento=CharField(max_length=50)
    ciudad=CharField(max_length=50)

class Estante(Model):
    id_bodega=ForeignKey(Bodega, on_delete=CASCADE)
    tipo=CharField(max_length=100, help_text="Tipo de estante: Tablero, Estante empotrado, Estante metálico")
    descripcion=TextField()
    dimensiones=CharField(max_length=50)


class Contenedor(Model):
    id_estante= ForeignKey(Estante, on_delete=CASCADE)
    tipo=CharField(max_length=100, help_text="Tipo de contenedor: Caneca plástica, estiba, balde")
    dimensiones=CharField(max_length=50)
    capacidad=FloatField()
    porcentaje_ocupacion=FloatField()

class Elemento(Model):
    id_producto=ForeignKey(Producto, on_delete=CASCADE)
    fecha_adquisición=DateField()
    estado=IntegerField()
    valor_compra=FloatField()
    id_bodega=ForeignKey(Bodega, on_delete=CASCADE)
    id_estante=ForeignKey(Estante, on_delete=CASCADE)