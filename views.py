import re
from django.shortcuts import render

from django.views import View

from django.http import HttpResponse

from django.template import loader


#Cargar los modelos que voy a emplear
from .models import Categoria
from .forms import CategoriaForm


# Create your views here.

class CategoriaView(View):

    def categoria(request):

        if request.method=="POST":

            formulario=CategoriaForm(request.POST)

            if formulario.is_valid():
                datos=formulario.cleaned_data            
                #TO DO: Aquí va el código para procesar el formulario
                #Guardar en una base de datos
                #Hacer algo con los datos del formulario
                return HttpResponse("Respuesta después de procesar el formulario")

        else:
            #Aquí va el código para mostrar el formulario
            formulario=CategoriaForm()
            datos={
                "form":formulario
            }
            return render(request,"CategoriaForm.html", datos)

    


class GestorDB(View):

    def get(self, request):
        #Crear un objeto con los datos del registro que se quiere ingresar
        categoria= Categoria(nombre="Oficina",descripcion="Artículo empleado en labores de oficina.")
        categoria.save()

        return (HttpResponse("registro insertado!!!"))


class Saludador(View):

    def get(self, request):

        plantilla= loader.get_template('saludo.html')

        datos={

            'mensaje':'Hola Mundo con Plantillas!!'
        }

        pagina=plantilla.render(datos)

        return (HttpResponse(pagina))

class CategoriaViewModelForm(View):

    def category_new(request):
        form = CategoriaForm()
        return render(request, 'CategoriaForm.html', {'form': form})
    